# wedding

> 体验二维码

![小程序优化后效果](https://images.gitee.com/uploads/images/2019/1106/161213_885f9c67_1937666.png "pic_010.png")

婚礼小程序转化版

![日记小程序](https://images.gitee.com/uploads/images/2019/1106/161339_19293632_1937666.jpeg "1570776962(1).jpg")

日记小程序

> 要定制小程序的加本人微信：huangbin910419

> A Mpvue project

## Build Setup

### 大家不要直接跑本项目，需要开通云开发，需要建立集合，上手前记得先熟悉云开发文档
### 结合掘金文章[项目讲解](https://juejin.im/post/5c341e1d6fb9a049f66c4876#heading-5)一起使用
### 禁止本源码作为商业用途

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
